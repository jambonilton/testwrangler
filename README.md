# Test Wrangler #

A simple heroku application for managing test cases and test workflow.

### How do I get set up? ###

1. Clone the repo.
2. Run main method wranger.WebApp to startup a server instance on port 8080.

### Dependencies ###

Dependencies are loaded directly in the repo and referenced via a local reposity configured in the pom.xml.  This is done because I don't want to publish 
the dependant jars to a public maven repo and I can run mvn install on a bunch of projects on my heroku server when that goes live.

* hresto: a little framework for RESTful Java apps on heroku
* jtree: a JSON parser and graph querying library
