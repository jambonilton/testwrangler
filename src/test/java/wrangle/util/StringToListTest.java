package wrangle.util;

import org.junit.Test;

import java.util.UUID;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

public class StringToListTest {

    final StringToList f = new StringToList();

    @Test
    public void test() {
        assertEquals(asList("foo", "bar"), f.apply("[foo, bar]"));
    }

}