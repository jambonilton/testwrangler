package wrangle.testcases;

import hresto.rest.NamedResource;
import hresto.sql.annotations.VarChar;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

public class TestCase extends NamedResource {

    protected final UUID directory;
    @VarChar(128)
    protected final String screen;
    @VarChar(256)
    protected final String story;
    @VarChar(512)
    protected final String description;
    @VarChar(32)
    protected final String isRegression;
    @VarChar(32)
    protected final String isAutomated;
    @VarChar(32)
    protected final String hasPassed;
    @VarChar(16)
    protected final String gusNumber;
    @VarChar(256)
    protected final List<String> tags;
    @VarChar(40)
    protected final String tester;

    public TestCase(UUID id, UUID creator, String name, Instant lastModified, UUID directory, String screen,
                    String story, String description, String isRegression, String isAutomated, String hasPassed,
                    String gusNumber, List<String> tags, String tester) {
        super(id, creator, name, lastModified);
        this.directory = directory;
        this.screen = screen;
        this.story = story;
        this.description = description;
        this.isRegression = isRegression;
        this.isAutomated = isAutomated;
        this.hasPassed = hasPassed;
        this.gusNumber = gusNumber;
        this.tags = tags;
        this.tester = tester;
    }

    public UUID getDirectory() {
        return directory;
    }

    public String getScreen() {
        return screen == null ? "screen" : screen;
    }

    public String getStory() {
        return story == null ? "story" : story;
    }

    public String getDescription() {
        return description == null ? "description" : description;
    }

    public String getIsRegression() {
        return isRegression == null ? "isRegression" : isRegression;
    }

    public String getIsAutomated() {
        return isAutomated == null ? "isAutomated" : isAutomated;
    }

    public String getHasPassed() {
        return hasPassed == null ? "hasPassed" : hasPassed;
    }

    public String getGusNumber() {
        return gusNumber == null ? "gusNumber" : gusNumber;
    }

    public String getTester() {
        return tester == null ? "unassigned" : tester;
    }

    @Override
    protected String getDefaultName() {
        return "New Test Case";
    }

}