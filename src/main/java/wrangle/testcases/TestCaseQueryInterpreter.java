package wrangle.testcases;

import hresto.auth.User;
import jtree.query.Comparison;
import jtree.query.Query;
import jtree.util.reflect.Fields;
import wrangle.directories.Directory;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static jtree.query.Query.*;

public class TestCaseQueryInterpreter implements Function<User, Function<Map<String, Deque<String>>, Query>> {

    final BiFunction<User, UUID, List<UUID>> subDirectoryProvider;

    public TestCaseQueryInterpreter(BiFunction<User, UUID, List<UUID>> subDirectoryProvider) {
        this.subDirectoryProvider = subDirectoryProvider;
    }

    @Override
    public Function<Map<String, Deque<String>>, Query> apply(User user) {
        return params -> apply(user, params);
    }

    public Query apply(User user, Map<String, Deque<String>> params) {
        if (params.isEmpty())
            return alwaysTrue();
        return Query.and(params.entrySet().stream()
                .map(e -> getTestCaseClause(user, e))
                .collect(toList()));
    }

    private Query getTestCaseClause(User user, Map.Entry<String, Deque<String>> e) {
        final String key = e.getKey();
        switch (key) {

            // Search all text fields
            case "search":
                final Collection<String> searchTerms = e.getValue();
                final List<Query> containsQueries = searchTerms.stream()
                        .flatMap(term -> getAllQueries(term))
                        .collect(toList());
                return or(containsQueries);

            // Search in all sub-directories, recursively
            case "directory":
                return in("directory", subDirectoryProvider.apply(user, UUID.fromString(e.getValue().getFirst())));

        }
        return e.getValue().size() == 1 ? is(e.getKey(), e.getValue().element()) : in(e.getKey(), e.getValue());
    }

    private Stream<Comparison> getAllQueries(String term) {
        return Fields.getAllFields(TestCase.class)
                .filter(field -> field.getType().equals(String.class) || field.getName().equals("tags"))
                .map(Field::getName)
                .map(key -> contains(key, term));
    }


}
