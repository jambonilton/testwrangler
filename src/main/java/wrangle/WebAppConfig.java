package wrangle;

import hresto.sql.SQLDatabase;
import org.hsqldb.Database;

public interface WebAppConfig {

    int getPort();
    SQLDatabase getDatabase();
    String getGoogleKey();

}
