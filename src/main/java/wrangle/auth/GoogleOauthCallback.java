package wrangle.auth;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import hresto.auth.SessionStore;
import hresto.auth.User;
import hresto.log.Logger;
import hresto.sql.DAO;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.handlers.form.FormParserFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

import static jtree.query.Query.is;

public class GoogleOauthCallback implements HttpHandler {

    private final static String ID_TOKEN_PARAM = "idtoken",
                                INVALID_TOKEN = "Invalid token.";

    private final NetHttpTransport transport = new NetHttpTransport();
    private final JsonFactory jsonFactory = new GsonFactory();
    private final Logger logger;
    private final String clientId;
    private final DAO<User> users;
    private final SessionStore sessionStore;

    public GoogleOauthCallback(Logger logger, String clientId, DAO<User> users, SessionStore sessionStore) {
        this.logger = logger;
        this.clientId = clientId;
        this.users = users;
        this.sessionStore = sessionStore;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {

        try {
            authenticate(exchange);
        } catch (IllegalArgumentException e) {
            exchange.setStatusCode(400);
            exchange.getResponseSender().send(INVALID_TOKEN);
        }
    }

    private void authenticate(HttpServerExchange exchange) throws IOException, GeneralSecurityException {
        final String idTokenString = getIdToken(exchange);
        if (exchange.isInIoThread()) {
            exchange.dispatch(this);
            return;
        }

        final GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
                .setAudience(Collections.singletonList(this.clientId))
                // Or, if multiple clients access the backend:
                //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
                .build();

        final GoogleIdToken idToken = verifier.verify(idTokenString);
        if (idToken != null) {
            final GoogleIdToken.Payload payload = idToken.getPayload();

            final User user = users.list(User.GOD_MODE, is("email", payload.getEmail())).findFirst()
                    .orElseGet(() -> createNewUser(payload));

            logger.log("User \"%s\" logged in.", user.getName());

            exchange.getResponseSender().send(sessionStore.create(user).getId());

        } else {
            logger.log("Invalid token "+idTokenString);
            exchange.setStatusCode(401);
            exchange.getResponseSender().send(INVALID_TOKEN);
        }
    }

    private User createNewUser(GoogleIdToken.Payload payload) {

        final String userId = payload.getSubject(),
                     email = payload.getEmail(),
                     name = (String) payload.get("name"),
                     pictureUrl = (String) payload.get("picture"),
                     locale = (String) payload.get("locale"),
                     familyName = (String) payload.get("family_name"),
                     givenName = (String) payload.get("given_name");

        final User user = new User(null, userId, email, name, pictureUrl, locale, familyName, givenName);
        users.save(User.GOD_MODE, user);
        return user;
    }

    private String getIdToken(HttpServerExchange exchange) throws IOException {
        final FormParserFactory.Builder builder = FormParserFactory.builder();
        final FormDataParser formDataParser = builder.build().createParser(exchange);
        if (formDataParser != null) {
            exchange.startBlocking();
            FormData formData = formDataParser.parseBlocking();
            return formData.getFirst(ID_TOKEN_PARAM).getValue();
        }
        throw new IllegalArgumentException("Expected token to be in form params!");
    }

}
