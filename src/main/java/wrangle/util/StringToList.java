package wrangle.util;

import java.util.List;
import java.util.function.Function;

import static java.util.Arrays.asList;

public class StringToList implements Function<String, List<String>> {

    @Override
    public List<String> apply(String s) {
        if (s.matches("\\[.*\\]"))
            s = s.substring(1, s.length()-1);
        return asList(s.split("\\s*,\\s*"));
    }

}
