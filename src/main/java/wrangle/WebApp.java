package wrangle;

import hresto.auth.SQLSessionStore;
import hresto.auth.SessionStore;
import hresto.auth.User;
import hresto.html.HtmlLoader;
import hresto.http.handlers.RefreshingHandler;
import hresto.http.handlers.StaticResponseHandler;
import hresto.log.Logger;
import hresto.log.Severity;
import hresto.rest.RestHandlerFactory;
import hresto.rest.RestRequestInterpreter;
import hresto.sql.DAO;
import hresto.sql.GenericDAOBuilder;
import hresto.sql.SQLDatabase;
import hresto.sql.exec.SQLExecutor;
import hresto.sql.hyper.HSQLDatabaseBuilder;
import hresto.sql.postgres.HerokuPostgresDatabase;
import hresto.sys.io.Resources;
import io.undertow.Undertow;
import io.undertow.UndertowOptions;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.BlockingHandler;
import io.undertow.server.handlers.PathHandler;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.server.handlers.resource.ResourceHandler;
import jtree.query.Query;
import jtree.util.io.json.Json;
import jtree.util.reflect.Conversion;
import wrangle.auth.GoogleOauthCallback;
import wrangle.directories.*;
import wrangle.testcases.TestCase;
import wrangle.testcases.TestCaseQueryInterpreter;
import wrangle.util.StringToList;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.nio.file.*;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static hresto.sql.gen.SQLQueryBuilder.in;
import static io.undertow.Handlers.path;
import static java.nio.file.StandardWatchEventKinds.*;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

public class WebApp {

    private static final WebAppConfig config = makeConfig();

    private static WebAppConfig makeConfig() {
        return new WebAppConfig() {
            @Override
            public int getPort() {
                return getEnv("PORT", 8080);
            }
            @Override
            public SQLDatabase getDatabase() {
                return isProdMode()
                        ? new HerokuPostgresDatabase()
                        : makeHSQLDB();
            }
            @Override
            public String getGoogleKey() {
                return "189794440642-6tck6qfbnbsrred8nqj3n1klltnsjv9h.apps.googleusercontent.com";
            }
            private boolean isProdMode() {
                return getEnv("PORT") != null;
            }
            private SQLDatabase makeHSQLDB() {
                return HSQLDatabaseBuilder.forFile("testdb").withScripts(
                        "hsql/users.sql", "hsql/sessions.sql", "hsql/schema/directory.sql", "hsql/schema/directory_share.sql",
                        "hsql/schema/directory_share_reference.sql", "hsql/schema/test_case.sql").build();
            }
            private <T> T getEnv(String key, T defaultValue) {
                final String str = System.getenv(key);
                return str == null ? defaultValue : (T) Conversion.convertTo(str, defaultValue.getClass());
            }
            private String getEnv(String key) {
                return System.getenv(key);
            }
        };
    }

    public static void main(String[] args) {

        final Logger logger = Logger.console();

        try {
            final InetAddress localhost = Inet4Address.getLocalHost();
            logger.log("Starting server on " + localhost + ", port " + config.getPort());

            initConversion();

            // refreshing handler to update app when sources change
            final RefreshingHandler handler = new RefreshingHandler(WebApp::createHandler);

            onChange("src/main/resources/web", handler::refresh);

            final Undertow server = Undertow.builder()
                    .setServerOption(UndertowOptions.ENABLE_HTTP2, true)
                    .setServerOption(UndertowOptions.ENABLE_SPDY, true)
                    .addHttpListener(config.getPort(), "0.0.0.0")
                    .setHandler(handler)
                    .build();
            server.start();
        } catch (Exception e) {
            logger.error(Severity.FATAL, e, "Failed to start server.");
        }

    }

    private static void initConversion() {
        Conversion.register(String.class, List.class, new StringToList());
    }

    private static void onChange(String directory, Runnable onChange) throws IOException {
        final WatchService watchService = FileSystems.getDefault().newWatchService();
        final List<WatchKey> watchKeys = watchKeys(watchService, directory).collect(Collectors.toList());
        Executors.newSingleThreadScheduledExecutor().scheduleWithFixedDelay(() -> {
            watchKeys.stream().flatMap(key -> key.pollEvents().stream()).findFirst().ifPresent(event -> onChange.run());
            watchKeys.forEach(WatchKey::reset);
        }, 1L, 1L, TimeUnit.SECONDS);
    }

    private static Stream<WatchKey> watchKeys(WatchService watchService, String filename) {
        try {
            final Path path = Paths.get(filename);
            if (!Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS))
                return Stream.empty();
            final WatchKey key = path.register(watchService, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
            return Stream.concat(Stream.of(key), Stream.of(path.toFile().listFiles()).flatMap(f -> watchKeys(watchService, f.getAbsolutePath())));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static PathHandler createHandler() {
        final Logger logger = Logger.console();
        final SQLDatabase database = config.getDatabase();
        final SQLExecutor sqlExecutor = new SQLExecutor(database);
        final SessionStore sessionStore = new SQLSessionStore(sqlExecutor);
        final HtmlLoader htmlLoader = new HtmlLoader("web");
        final ExecutorService threadPool = Executors.newCachedThreadPool();
        final Json json = new Json();
        final RestHandlerFactory restFactory = new RestHandlerFactory(threadPool, json, logger, sessionStore, database);
        final AccessibleDirectoriesProvider directoryAccess = new AccessibleDirectoriesProvider(sqlExecutor);
        final DAO<Directory> directoryDAO = GenericDAOBuilder.forType(Directory.class)
                .setAccessConstraint(user -> in("id", directoryAccess.apply(user)))
                .withDatabase(database).build();
        final Function<User, Function<Map<String, Deque<String>>, Query>> testCaseQueryInterpreter = new TestCaseQueryInterpreter(
                new SubDirectoryProvider(directoryDAO));

        return path()
                .addExactPath("/", new StaticResponseHandler(htmlLoader.load("index.html"), "text/html"))
                .addExactPath("/script.js", mergingResourcesHandler("application/javascript", "web/js",
                        "cookies.js", "data-importer.js", "params.js", "list-hierarchizer.js", "view-model.js",
                        "api-endpoint.js", "arrays.js", "dom-helpers.js", "ajax.js", "models/user-model.js", "models/directory-model.js",
                        "models/testcase-model.js", "models/directory-hierarchy-model.js", "models/chart-model.js"))
                .addExactPath("/vendor.js", mergingResourcesHandler("application/javascript", "web/js/vendor",
                        "papaparse.min.js"))
                .addExactPath("/style.css", mergingResourcesHandler("text/css", "web/css", "general.css",
                        "header.css", "directories.css", "testcases.css", "controls.css", "charts.css"))
                .addExactPath("/token-signin", new GoogleOauthCallback(logger, config.getGoogleKey(), GenericDAOBuilder.forType(User.class).withDatabase(database).build(), sessionStore))
                .addPrefixPath("/static", new ResourceHandler(new ClassPathResourceManager(WebApp.class.getClassLoader(), "web/static")))
                .addPrefixPath("/api/users", restFactory.create("/api/users", User.class))
                .addPrefixPath("/api/directories", restFactory.create("/api/directories", Directory.class))
                .addPrefixPath("/api/testcases", restFactory.create("/api/testcases", new RestRequestInterpreter<>(json, TestCase.class, testCaseQueryInterpreter)))
                .addPrefixPath("/api/share", new BlockingHandler(new DirectoryShareHandler(sessionStore, sqlExecutor,
                        directoryDAO, GenericDAOBuilder.forType(DirectoryShareReference.class).withDatabase(database).build())));
    }

    public static HttpHandler mergingResourcesHandler(String contentType, String path, String... resources) {
        final Map<String, Path> paths = Resources.getFilesInPackage(path)
                .collect(toMap(p -> p.toString().split(path+'/')[1], identity()));
        final String fullContent = Stream.of(resources)
                .filter(paths::containsKey)
                .map(paths::get)
                .map(Resources::toString)
                .collect(joining("\n\n"));
        return new StaticResponseHandler(fullContent, contentType);
    }

}
