package wrangle.directories;

import hresto.auth.User;
import hresto.sql.exec.SQLExecutor;
import hresto.sql.gen.SQLQueryable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.UUID;
import java.util.function.Function;

import static hresto.sql.gen.SQLQueryBuilder.eq;
import static hresto.sql.gen.SQLQueryBuilder.or;
import static hresto.sql.gen.SQLQueryBuilder.select;
import static java.util.stream.Collectors.toList;

public class AccessibleDirectoriesProvider implements Function<User, Collection<UUID>> {

    final SQLExecutor db;

    public AccessibleDirectoriesProvider(SQLExecutor db) {
        this.db = db;
    }

    @Override
    public Collection<UUID> apply(User user) {
        final SQLQueryable query = select("id")
                .from("Directory").leftJoin("DirectoryShare", "id", "directory")
                .where(or(eq("creator", user.getId()), eq("sharee", user.getId())));
        return db.query(query, this::getUUIDFromRow).collect(toList());
    }

    private UUID getUUIDFromRow(ResultSet rs) throws SQLException {
        return UUID.fromString(rs.getString(1));
    }

}
