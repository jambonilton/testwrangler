package wrangle.directories;

import hresto.rest.BasicResource;

import java.time.Instant;
import java.util.UUID;

public class DirectoryShareReference extends BasicResource {

    protected final Instant created;
    protected final UUID directory;

    public DirectoryShareReference(UUID id, Instant created, UUID directory) {
        super(id);
        this.created = created;
        this.directory = directory;
    }

    public Instant getCreated() {
        return created;
    }

    public UUID getDirectory() {
        return directory;
    }

}
