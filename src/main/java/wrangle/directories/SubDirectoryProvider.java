package wrangle.directories;

import hresto.auth.User;
import hresto.rest.BasicResource;
import hresto.sql.DAO;
import jtree.query.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.BiFunction;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class SubDirectoryProvider implements BiFunction<User, UUID, List<UUID>> {

    protected final DAO<Directory> dao;

    public SubDirectoryProvider(DAO<Directory> dao) {
        this.dao = dao;
    }

    @Override
    public List<UUID> apply(User user, UUID directory) {
        final List<Directory> allAccessible = dao.list(user, Query.alwaysTrue()).collect(toList());
        final List<UUID> result = new ArrayList<>();
        List<UUID> currentGen = asList(directory);
        while (!currentGen.isEmpty()) {
            result.addAll(currentGen);
            currentGen = getNextGen(allAccessible, currentGen);
        }
        return result;
    }

    private List<UUID> getNextGen(List<Directory> all, List<UUID> previousGen) {
        return all.stream()
                .filter(dir -> previousGen.contains(dir.getParent()))
                .map(BasicResource::getId)
                .collect(toList());
    }

    private List<UUID> getIds(List<Directory> currentGen) {
        return currentGen.stream()
                .map(BasicResource::getId)
                .collect(toList());
    }
}
