package wrangle.directories;

import hresto.rest.NamedResource;

import java.time.Instant;
import java.util.UUID;

public class Directory extends NamedResource {

    protected final UUID parent;

    public Directory(UUID id, UUID creator, String name, Instant lastModified, UUID parent) {
        super(id, creator, name, lastModified);
        this.parent = parent;
    }

    public UUID getParent() {
        return parent;
    }

    @Override
    protected String getDefaultName() {
        return "new directory";
    }

}
