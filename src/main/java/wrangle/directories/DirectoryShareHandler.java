package wrangle.directories;

import hresto.auth.Session;
import hresto.auth.SessionStore;
import hresto.auth.User;
import hresto.sql.DAO;
import hresto.sql.exec.SQLExecutor;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.Cookie;
import io.undertow.util.HttpString;

import java.time.Instant;
import java.util.UUID;

import static hresto.sql.gen.SQLInsertBuilder.insertInto;

public class DirectoryShareHandler implements HttpHandler {

    private static final HttpString POST = HttpString.tryFromString("POST");
    public static final int FOUND = 302;
    public static final HttpString LOCATION = HttpString.tryFromString("Location");

    protected final SessionStore sessionStore;
    protected final SQLExecutor sql;
    protected final DAO<Directory> directories;
    protected final DAO<DirectoryShareReference> references;

    public DirectoryShareHandler(SessionStore sessionStore,
                                 SQLExecutor sql,
                                 DAO<Directory> directories,
                                 DAO<DirectoryShareReference> references) {
        this.sessionStore = sessionStore;
        this.directories = directories;
        this.references = references;
        this.sql = sql;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {final Cookie sessionCookie = exchange.getRequestCookies().get("Session");
        if (sessionCookie == null)
            throw new IllegalAccessException("Unauthorized access to data endpoint.  Session header required.");
        final User user = sessionStore.get(sessionCookie.getValue()).map(Session::getUser)
            .orElseThrow(() -> new IllegalAccessException("No session available for provided token."));
        if (exchange.getRequestMethod().equals(POST))
            insertDirectoryShareReference(exchange, user);
        else
            addAccessForUser(exchange, user);
    }

    private void insertDirectoryShareReference(HttpServerExchange exchange, User user) {
        try {
            final UUID directoryId = UUID.fromString(exchange.getQueryParameters().get("directory").element());
            // ensure directory is accessible
            directories.get(user, directoryId);
            final DirectoryShareReference share = new DirectoryShareReference(null, Instant.now(), directoryId);
            references.save(user, share);
            exchange.getResponseSender().send(share.getId().toString());
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void addAccessForUser(HttpServerExchange exchange, User user) {
        try {
            final String referenceParameter = exchange.getQueryParameters().get("ref").element();
            final UUID referenceId = UUID.fromString(referenceParameter);
            final DirectoryShareReference reference = references.get(user, referenceId);
            sql.apply(insertInto("DirectoryShare", "directory", "sharee").values(reference.getDirectory(), user.getId()));
            // redirect user to main page
            exchange.setStatusCode(FOUND);
            exchange.getResponseHeaders().put(LOCATION, exchange.getRequestScheme() + "://" + exchange.getHostAndPort() + "/?directory="+reference.getDirectory());
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

}
