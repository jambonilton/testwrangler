package wrangle.directories;

import hresto.rest.BasicResource;

import java.util.UUID;

public class DirectoryShare extends BasicResource {

    protected final UUID directory, sharee;

    public DirectoryShare(UUID id, UUID directory, UUID sharee) {
        super(id);
        this.directory = directory;
        this.sharee = sharee;
    }

    public UUID getDirectory() {
        return directory;
    }

    public UUID getSharee() {
        return sharee;
    }

}
