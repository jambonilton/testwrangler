CREATE TABLE Directory (
    id UUID PRIMARY KEY,
    creator UUID NOT NULL REFERENCES AppUser(id),
    name VARCHAR(128) NOT NULL,
    lastModified TIMESTAMP NOT NULL,
    parent UUID DEFAULT NULL REFERENCES Directory(id)
);