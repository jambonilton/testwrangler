CREATE TABLE DirectoryShareReference (
    id UUID PRIMARY KEY,
    created TIMESTAMP NOT NULL,
    directory UUID NOT NULL REFERENCES Directory(id)
);