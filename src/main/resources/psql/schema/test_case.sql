CREATE TABLE TestCase (
    id UUID PRIMARY KEY,
    creator UUID NOT NULL REFERENCES AppUser(id),
    name VARCHAR(128) NOT NULL,
    lastModified TIMESTAMP NOT NULL,
    directory UUID NOT NULL REFERENCES Directory(id),
    screen VARCHAR(128) NOT NULL,
    story VARCHAR(256) NOT NULL,
    description VARCHAR(512) NOT NULL,
    isRegression VARCHAR(32) NOT NULL,
    isAutomated VARCHAR(32) NOT NULL,
    hasPassed VARCHAR(32) NOT NULL,
    gusNumber VARCHAR(16) NOT NULL,
    tags VARCHAR(256) DEFAULT NULL,
    tester VARCHAR(40) DEFAULT NULL
);