CREATE TABLE DirectoryShare (
    directory UUID NOT NULL REFERENCES Directory(id),
    sharee UUID NOT NULL REFERENCES AppUser(id),
    PRIMARY KEY (directory, sharee)
);