CREATE TABLE Session (
    id VARCHAR(42) PRIMARY KEY,
    sessionUser UUID NOT NULL REFERENCES AppUser(id)
);