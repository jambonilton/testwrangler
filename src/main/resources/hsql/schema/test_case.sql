CREATE TABLE TestCase (
    id VARCHAR(36) PRIMARY KEY,
    creator VARCHAR(36) NOT NULL REFERENCES AppUser(id),
    name VARCHAR(128) NOT NULL,
    lastModified TIMESTAMP NOT NULL,
    directory VARCHAR(36) NOT NULL REFERENCES Directory(id),
    screen VARCHAR(128) NOT NULL,
    story VARCHAR(256) NOT NULL,
    description VARCHAR(512) NOT NULL,
    isRegression VARCHAR(24) NOT NULL,
    isAutomated VARCHAR(24) NOT NULL,
    hasPassed VARCHAR(24) NOT NULL,
    gusNumber VARCHAR(16) NOT NULL,
    tags VARCHAR(256) DEFAULT NULL,
    tester VARCHAR(36) DEFAULT NULL
);