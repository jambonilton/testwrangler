CREATE TABLE DirectoryShareReference (
    id VARCHAR(36) PRIMARY KEY,
    created TIMESTAMP NOT NULL,
    directory VARCHAR(36) NOT NULL REFERENCES Directory(id)
);