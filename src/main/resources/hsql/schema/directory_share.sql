CREATE TABLE DirectoryShare (
    directory VARCHAR(36) NOT NULL REFERENCES Directory(id),
    sharee VARCHAR(36) NOT NULL REFERENCES AppUser(id),
    PRIMARY KEY (directory, sharee)
);