var ajax = {
    send: function(request) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open(request.method, request.url, true);
            xhr.onload = resolve;
            xhr.onerror = reject;
            if (request.body) {
                xhr.send(request.body);
            } else {
                xhr.send();
            }
        });
    },
    get: function(request) {
        return this.send(request);
    },
    post: function(request) {
        request.method = 'POST';
        return this.send(request);
    }
};