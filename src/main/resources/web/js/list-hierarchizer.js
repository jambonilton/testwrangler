var listHierarchizer = {
    hierarchize: function(flatList) {
        if (flatList === null || flatList.length === 0)
            return;

        let root = this.findRoot(flatList);
        this.addChildren(flatList, root);
        return root;
    },
    findRoot: function(flatList) {
        return flatList.find(item => !item.parent);
    },
    addChildren: function(flatList, parent) {
        parent.children = flatList.filter(item => parent.id === item.parent);
        parent.children.forEach(item => this.addChildren(flatList, item));
    },
    findItem: function(flatList, itemToFind) {
        return flatList.find(item => item.id === itemToFind.id);
    },
    findPath: function(flatList, item) {
        let path = [item];
        let currentPredecessor = item;
        while(currentPredecessor.parent) {
            path.push(currentPredecessor.parent);
            currentPredecessor = currentPredecessor.parent;
        }
        path.reverse();
        return path;
    }
};