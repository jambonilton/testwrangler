Array.prototype.distinct = function() {
    return [...new Set(this)];
};
Array.prototype.toObject = function(keyFunction, valueFunction) {
    let obj = {};
    this.forEach(item => obj[keyFunction(item)] = valueFunction(item));
    return obj;
};