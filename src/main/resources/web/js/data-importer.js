var dataImporter = {
    import: function(files) {
        if (files.length === 0) {
            alert("Please select a file for import.");
            return;
        }

        let file = files[0];
        if (file.type !== "text/csv") {
            alert("Only CSV files are supported.");
            return;
        }

        Papa.parse(file, { complete: this.load.bind(this) });
    },
    getModel: function(propertyMapping, csvRow) {
        let model = new TestCaseModel();
        model["directory"] = localStorage.directory;
        for (var key in propertyMapping)
            model[key] = csvRow[propertyMapping[key]];
        return model;
    },
    getPropertyMapping: function() {
        // TestCaseModel property => CSV column index
        return {
            "screen": 0,
            "story": 1,
            "name": 2,
            "description": 3,
            "isRegression": 4,
            "isAutomated": 5,
            "hasPassed": 6,
            "gusNumber": 10
        };
    },
    load: function(result) {
        if (result.errors.length > 0)
            alert("Invalid CSV file.");

        let header = result.data.shift();
        let mapping = this.getPropertyMapping();

        let testcasesEndpoint = new RestEndpoint('/api/testcases', TestCaseModel, localStorage.token);
        for (let i = 0; i < result.data.length; i++)
            testcasesEndpoint.save(this.getModel(mapping, result.data[i]));
    }
};