
/*
 * ================== VIEW MODEL MAPPINGS ==================
 * Add special keywords for bindings through this object.
 * =========================================================
 */
var ViewModelMappings = {

    // Clone element for every element within given array
    each: function(element, field) {
        var array = this[field];
        if (!array) {
            element.style.display = 'none';
            return;
        } else if (typeof array === 'function') {
            array = array.call(this);
        }
        let cleanRender = function() {
            let parent = element.parentNode;
            let toRemove = [];
            for (let sibling of parent.children) {
                if (sibling.originator === element)
                    toRemove.push(sibling);
            }
            for (let e of toRemove)
                parent.removeChild(e);

            element.style.display = '';

            for (var i=0; i < array.length; i++) {

                let newNode = element.cloneNode(true);
                newNode.originator = element;
                newNode.removeAttribute('bind');
                element.parentNode.insertBefore(newNode, element);

                if (typeof array[i].bindTo === 'function') {
                    array[i].bindTo(newNode);
                } else {
                    newNode.innerHTML = array[i] + newNode.innerHTML;
                }
            }
            element.style.display = 'none';
        }
        let updateRender = function() {
            var i=0, node = element.previousSibling;

            for (; i < array.length && node.originator == element; i++) {
                node = node.previousSibling;
            }
            element.style.display = '';

            for (; i < array.length; i++) {

                let newNode = element.cloneNode(true);
                newNode.originator = element;
                newNode.removeAttribute('bind');
                element.parentNode.insertBefore(newNode, element);

                if (typeof array[i].bindTo === 'function') {
                    array[i].bindTo(newNode);
                } else {
                    newNode.innerHTML = array[i] + newNode.innerHTML;
                }
            }
            element.style.display = 'none';
        }
        // override push etc to perform render
        array.push = function(item) {
            Array.prototype.push.call(this, item);
            updateRender();
        };
        array.unshift = function(item) { // TODO do update render
            Array.prototype.unshift.call(this, item);
            cleanRender();
        };
        array.splice = function(index, count) {
            let length = array.length;
            Array.prototype.splice.call(this, index, count);

            let node = element.previousSibling;
            for (let i=0; i < length-(index+count); i++) {
                node = node.previousSibling;
            }
            for (let j=0; j < count; j++) {
                let toDelete = node;
                node = node.previousSibling;
                node.parentNode.removeChild(toDelete);
            }
        };
        return cleanRender();
    },

    // Display escaped text value, also good for contenteditable
    text: function(element, field) {
        let text = this[field]+'' || '';
        if (text.includes('\n')) {
            text.split('\n').forEach(line => {
                let div = document.createElement('div');
                if (line)
                    div.textContent = line;
                else
                    div.innerHTML = '<br>';
                element.appendChild(div);
            });
        }
        else if (text) {
            element.textContent = text;
        }
    },

    'if': function(element, field) {
        element.style.display = this[field] ? '' : 'none';
    },

    'ifnot': function(element, field) {
        element.style.display = this[field] ? 'none' : '';
    }

};

/*
 * ================================ VIEW MODEL =====================================
 * Base class for any object intending to use bindTo(element) two-way binding magic.
 * =================================================================================
 */
class ViewModel {

    constructor(obj) {
        Object.defineProperty(this, '_watchers', {
            enumerable: false,
            value: {}
        });
        for (let property in obj) {
            this.defineProperty(property, obj[property]);
        }
    }

    defineProperty(property, value) {
        if (this.hasOwnProperty(property)) {
            this['_'+property] = value;
            return;
        }
        else if (typeof value === 'function') {
            this[property] = value;
            return;
        }
        Object.defineProperty(this, '_'+property, {
            enumerable: false,
            writable: true,
            value: value
        });
        Object.defineProperty(this, property, {
            enumerable: true,
            get: function() {
                return this['_'+property];
            },
            set: function(value) {
                this['_'+property] = value;
                if (typeof this._watchers[property] === 'function')
                    this._watchers[property]();
                if (typeof this._watchers['*'] === 'function')
                    this._watchers['*']();
            }
        });
    }

    isContentEditable(element, property) {
        return element.attributes['contenteditable'] && ['text','innerHTML'].includes(property);
    }

    isInput(element, property) {
        return ['TEXTAREA','INPUT'].includes(element.nodeName) && property === 'value';
    }

    bindTo(selection) {
        let elementArray = selection instanceof Node ? [selection] : document.querySelectorAll(selection);
        elementArray.forEach(element => {
            let bindExpression = element.attributes
                && element.attributes['bind']
                && element.attributes['bind'].value;
            if (bindExpression) {
                bindExpression.split(/\s*;\s*/).forEach(assignment => {

                    let [key, value] = assignment.split(/\s*=\s*/);

                    this.syncFields(element, key, value);

                    if (this.isContentEditable(element, key))
                        element.oninput = () => this[value] = Array.from(element.childNodes).map(line => line.textContent).join('\n').trim();
                    else if (this.isInput(element, key))
                        element.oninput = () => this[value] = element.value;
                    else
                        this.onChange(value, this.syncFields.bind(this, element, key, value));

                });
            }
            if (element instanceof Element) {
                let childNodes = [...element.childNodes];
                childNodes.forEach(child => this.bindTo(child));
            }
        });
        return this;
    }

    syncFields(element, elementField, thisField) {
        try {
            this.defineProperty(thisField, this[thisField]);

            if (ViewModelMappings[elementField])
                ViewModelMappings[elementField].call(this, element, thisField);
            else if (typeof this[thisField] === 'function')
                element[elementField] = elementField.startsWith('on') ? () => this[thisField]() : this[thisField]();
            else
                element[elementField] = this[thisField];

        } catch (e) {
            console.warn('Failed to sync field "'+elementField+'" on '+element+' " with object field "'+thisField+'".', e);
        }
    }

    onChange(property, callback) {
        if (typeof this[property] === 'function')
            property = '*';
        if (typeof this._watchers[property] === 'function') {
            let originalCallback = this._watchers[property];
            this._watchers[property] = () => {
                originalCallback();
                callback()
            };
        }
        else
            this._watchers[property] = callback;
    }

    triggerChange(property) {
        if (typeof this._watchers[property] === 'function')
            this._watchers[property]();
    }

}