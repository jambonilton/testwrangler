class TestCaseModel extends ViewModel {

    constructor(obj) {
        super(obj);
    }

    gusLink() {
        return "https://gus.my.salesforce.com/apex/ADM_WorkLocator?bugorworknumber=" + this.gusNumber;
    }

    removeTag(tag) {
        this.tags.splice(this.tags.indexOf(tag), 1);
        if (typeof this.save === 'function') {
            this.save();
        }
    }

}