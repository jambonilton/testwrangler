let getSelectedDirectoryFromWindow = function() {
    let paramValue = getParameter('directory');
    return paramValue ? localStorage.directory = paramValue : localStorage.directory;
};

class DirectoryHierarchyModel extends ViewModel {

    constructor(directories) {
        super({
            directories: directories,
            selectedId: getSelectedDirectoryFromWindow()
        });
        this.populateChildren();
    }

    populateChildren() {
        let self = this;
        this.directories.forEach(dir => {
            Object.defineProperty(dir, 'siblings', {
                enumerable: false,
                value: self.addPersistence(self.directories.filter(bro => dir.parent === bro.parent && bro !== dir), { parent: dir.parent })
            });
        });
    }

    selectedChain() {
        let selected = this.getSelected();
        if (!selected)
            return this.addPersistence([]);
        let selectChain = [selected];
        var parent = this.directories.find(dir => dir.id === selected.parent);
        while (parent) {
            selectChain.unshift(parent);
            parent = this.directories.find(dir => dir.id === parent.parent);
        }
        return this.addPersistence(selectChain, { parent: selected } );
    }

    selectedChildren() {
        let selectedId = this.getSelectedId();
        return this.addPersistence(this.directories.filter(child => child.parent === selectedId), { parent: selectedId });
    }

    getSelectedId() {
        return this.selectedId;
    }

    getSelected() {
        let selectedId = this.getSelectedId();
        return this.directories.find(dir => dir.id === selectedId);
    }

    setSelectedId(directoryId) {
        localStorage.directory = directoryId;
        this.selectedId = directoryId;
    }

    addPersistence(dirs, opts) {
        dirs.save = this.directories.save.bind(this.directories, opts);
        return dirs;
    }

    rootsNodes() {
        return this.directories.filter(dir => !dir.parent);
    }

    save() {
        return this.directories.save({ parent: this.getSelectedId() });
    }

}