

class OptionModel extends ViewModel {

    constructor(parent, onSelect, value) {
        super({
            select: onSelect.bind(parent, value),
            value: value
        });
    }

}

class DataPoint extends ViewModel {

    constructor(onSelect, key, value) {
        super({
            select: onSelect,
            key: key,
            value: value
        });
    }

}

const aggregations = ['count','avg','sum'];

class ChartModel extends ViewModel {

    constructor(parent, entity, fields, query) {
        super({
            parent: parent,
            entity: entity,
            aggregation: aggregations[0],
            field: fields[0],
            query: query,
            data: []
        });
        this.aggregations = aggregations.map(a => new OptionModel(this, this.chooseAggregation, a));
        this.fields = fields.map(f => new OptionModel(this, this.chooseField, f));
        this.onChange('aggregation', this.refresh.bind(this));
        this.onChange('field', this.refresh.bind(this));
        this.refresh();
    }

    chooseAggregation(value) {
        this.aggregation = value;
    }

    chooseField(value) {
        this.field = value;
    }

    remove() {
        this.parent.charts.splice(this.parent.charts.indexOf(this), 1);
        if (this.parent.filters[this.field]) {
            delete this.parent.filters[this.field];
            this.parent.refresh();
        }
    }

    refresh() {
        var url = '/api/'+this.entity+'/'+this.aggregation+'_by_'+this.field;
        let query = this.parent.getQuery();
        if (query) {
            url += '?' + Object.entries(query)
                .filter(e => typeof e[1] !== 'undefined')
                .map(e => e[0] + '=' + encodeURIComponent(e[1]))
                .join('&');
        }
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.onload = this.load.bind(this, xhr);
        // xhr.onerror = () => reject(self.onerror(xhr)); // TODO error handling's for suckas!
        xhr.send();
    }

    load(xhr) {
        if (xhr.status >= 200 && xhr.status < 300) {
            // for (var i = this.data.length; i > 0; i--)
            //     this.data.pop();
            this.data = [];
            let dataArray = JSON.parse(xhr.responseText);
            dataArray.splice(0, 1); // ignore field declaration
            dataArray.sort((e1, e2) => e2[1] - e1[1]);
            dataArray.forEach(e => this.data.push(new DataPoint(this.parent.filter.bind(this.parent, this.field, e[0]), e[0], e[1])));
        }
    }


}